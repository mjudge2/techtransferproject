from bs4 import BeautifulSoup
from urllib2 import urlopen
from HTMLParser import HTMLParser
import re
import sys
import logging

BASE_URL = sys.argv[3]

def get_category_links(section_url):
	category_map = {}
	html = urlopen(section_url).read()
	soup = BeautifulSoup(html, "lxml")
	li_category = soup.findAll("li", "category")
	for listItem in li_category:
		category_map[str(listItem.a.string)] = listItem.a["href"]

	return category_map

def get_technology_links(category_map):
	technology_links = {}
	for category in category_map:

		url_suffix = category_map[category]

		html = urlopen(BASE_URL +  url_suffix)
		soup = BeautifulSoup(html, "lxml")

		##find all pages
		pages = [category_map[category]]
		pages += find_page_links(soup)

		##find technology links for that page
		technology_links[category] = get_technologies(soup)

	return technology_links

def find_page_links(html_soup):
	pages = html_soup.find("div", "pages")

	page_links = []
	for page in pages.findAll("a"):
		page_links.append(page["href"])

	return page_links

def get_technologies(html_soup):
	div_tech = html_soup.findAll("div", "technology")
	technologies = []
	for tech in div_tech:
		for a in tech.findAll('a'):
			link = a["href"]
			if link not in technologies:
				technologies.append(link)
			else:
				continue

	return technologies

def loop_through_all_pages():
	html = urlopen(BASE_URL + '/technologies').read()
	html_soup = BeautifulSoup(html, "lxml")

	pages = html_soup.find("div", "pages")
	page_links = ['/technologies']
	for page in pages.findAll("a"):
		page_links.append(page["href"])

	return page_links


def get_technology_information(technology_links):
	#open file
	logging.basicConfig(filename='nouvant_warnings.log', level=logging.DEBUG)
	print "Opening the file..."
	file = open(sys.argv[2], 'w')
	num_added = 0

	for category in technology_links:
		links_list = technology_links[category]
		for link in links_list:
			logging.warning(link)
			html = urlopen(BASE_URL + link)
			soup = BeautifulSoup(html, "lxml")

			tech_name = ""
			internal_number = ""
			managed_by = []
			inventors = []
			publications = []
			categories = []
			# categories.append(category)
			technology = []
			advantages = []
			solution = []
			applications = []
			problem_addressed = []
			stage_of_development = []
			data = ""
			ip = []

			div_tech = soup.findAll("div", "technology")
			for section in div_tech:
				try:
					tech_name = str(section.h1.string.encode('utf-8', 'ignore'))
					internal_number = str(section.em.string)
				except AttributeError:
					logging.warning("couldn't find title")

				try:
					managers = section.findAll("dd", "manager")
					for man in managers:
						managed_by.append(str(man.a.string.encode('utf-8', 'ignore')))
				except AttributeError:
					logging.warning("couldn't find managers")

				try:
					dd_inv = section.findAll("dd", "inventor")
					for inventor in dd_inv:
						inventors.append(str(inventor.a.string.encode('utf-8', 'ignore')))
				except AttributeError:
					logging.warning("couldn't find inventors")

				try:
					pubs = section.findAll("dd", "publication")
					for pub in pubs:
						publications.append({str(pub.a.cite.string.encode('utf-8', 'ignore')): pub.a["href"]})
				except Exception:
					logging.warning("couldn't find publications")
					continue

				try:
					li_category = section.findAll("li", "category")
					for cat in li_category:
						category = cat.a.string
						if category not in categories:
							categories.append(str(category.encode('utf-8', 'ignore')))
				except AttributeError:
					logging.warning("couldn't find category")

				# search for dd link
				## extract dd.a[href]
				try:
					external_links = section.findAll("dd", "link")
					for l in external_links:
						if 'patent' in l.a.text.lower():
							ip.append(l.a["href"])
				except Exception:
					logging.warning("couldn't find IP")

				# try:
				# 	pats_header = section.findAll("dt", "patents")
				# 	pats = pats_header[0].next_sibling
				# 	ip.append(str(pats.em.string.encode('utf-8', 'ignore')))
				# except Exception:
				# 	print "couldn't find IP"

				try:
					curr_section = section.findAll("div", "information")
					if len(curr_section) > 0:
						curr_section[0]
						a_sections = curr_section[0].findAll("a")
						for item in a_sections:
							try:
								if 'patent' in (item.string.encode('utf-8', 'ignore')).lower():
									ip.append(str(item.string.encode('utf-8', 'ignore')))
							except Exception:
								continue
				except AttributeError:
					logging.warning("couldn't find IP")

				try:
					content = section.findAll("div", "content")
					data = BeautifulSoup(str(content), "lxml").text
					for c in content:
						headers = c.findAll("h2")
						for header in headers:
							lower_header = ""
							try:
								lower_header = str(header.string.encode('utf-8', 'ignore')).lower()
							except AttributeError:
								h = []
								h += header.stripped_strings
								if len(h) > 0:
									lower_header = str(h[0].encode('utf-8', 'ignore')).lower()

							if 'publication' in lower_header:
								next_content = header.next_sibling
								try:
									if re.search('[a-zA-Z]', next_content) == None:
										next_content = next_content.next_sibling
								except TypeError:
									next_content = next_content.next_sibling
								a_sections = next_content.findAll("a")
								for item in a_sections:
									try:
										publications.append({str(item.string.encode('utf-8', 'ignore')): item["href"]})
									except Exception:
										continue

				except AttributeError:
					logging.warning("couldn't find body")

			tech_link = BASE_URL+link
			print_string = "({univ}, {url}, {int_num}, {tech_name}, {managed_by}, {inventors}, {publications}, {categories}, {ip}, {data})\n".format(
					univ=sys.argv[1], url=tech_link, int_num=internal_number, tech_name=tech_name, managed_by=managed_by,
					inventors=inventors, publications=publications, categories=categories, ip=ip,
					data=data)

			num_added += 1

			file.write(print_string)

	file.close()
	print num_added
	return


def find_ip(dd):
	while True:
		dd.find_next()

def extract_after(header, headers):
	curr = header
	after_header = []

	while curr.next_sibling not in headers:
		if curr.next_sibling.name == 'ul':
			list_items = []
			list_items = curr.next_sibling.findAll("li")

			if len(list_items) > 0 and len(list_items[0].contents) > 1:
				list_items = curr.next_sibling.findAll("span")

			for item in list_items:
				adv = str(item.getText().encode('utf-8', 'ignore'))
				if adv not in after_header and adv != "":
					after_header.append(adv)
			break
		else:
			try:
				after_header.append(curr.next_sibling.getText())
			except AttributeError:
				after_header.append(curr.next_sibling)

			curr = curr.next_sibling

	return after_header

def extract_technologies_from_pages(pages):
	technologies = []
	for page in pages:
		html = urlopen(BASE_URL + page)
		html_soup = BeautifulSoup(html, "lxml")
		techs = html_soup.findAll("div", "technology")
		for tech in techs:
			technologies.append(tech.h2.a["href"])
	return technologies


def main():
	pages = loop_through_all_pages()
	technologies = extract_technologies_from_pages(pages)
	get_technology_information({"": technologies})

if __name__ == "__main__":
	main()