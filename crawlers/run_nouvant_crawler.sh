#!/bin/sh

python nouvantcrawler.py "American University Cairo" "am_univ_cairo.txt" "http://innovations.aucegypt.edu"
python nouvantcrawler.py "Columbia" "columbia.txt" "http://innovation.columbia.edu"
python nouvantcrawler.py "Florida Atlantic University" "florida_atlantic.txt" "http://inventions.fau.edu"
python nouvantcrawler.py "Georgia Tech" "georgia_tech.txt" "http://technologies.gtrc.gatech.edu"
python nouvantcrawler.py "George Washington University" "gw.txt" "http://technologies.research.gwu.edu"
python nouvantcrawler.py "Mayo Clinic" "mayoclinic.txt" "http://technologies.ventures.mayoclinic.org"
python nouvantcrawler.py "MIT" "mit.txt" "http://technology.mit.edu"
python nouvantcrawler.py "North Carolina State University" "nc_state.txt" "http://licensing.research.ncsu.edu"
python nouvantcrawler.py "University of Arizona" "u_arizona.txt" "http://inventions.arizona.edu"
python nouvantcrawler.py "University of Central Florida" "u_central_florida.txt" "http://technologies.tt.research.ucf.edu"
python nouvantcrawler.py "University of Florida" "u_florida.txt" "http://technologylicensing.research.ufl.edu"
python nouvantcrawler.py "University of Michigan" "u_michigan.txt" "http://inventions.umich.edu"
python nouvantcrawler.py "University of Minnesota" "u_minnesota.txt" "http://license.umn.edu"

