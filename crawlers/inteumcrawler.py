# http://jhu.technologypublisher.com/searchresults.aspx?q=&page=0&sort=datecreated&order=desc

from bs4 import BeautifulSoup
from urllib2 import urlopen
from HTMLParser import HTMLParser
import re
import sys
import logging


BASE_URL = sys.argv[3]
# "http://upenn.technologypublisher.com"
UNIVERSITY = sys.argv[1]
# "University of Pennsylvania"
def loop_through_all_pages():
	technology_links = []
	page_count = 0
	try:
		while page_count > -1:
			print str(page_count)
			tech_page_url = BASE_URL + "/searchresults.aspx?q=&page=" + str(page_count) + "&sort=datecreated&order=desc"
			html = urlopen(tech_page_url).read()
			html_soup = BeautifulSoup(html, "lxml")
			c_content = html_soup.findAll('div', 'c_content')[0]
			links = extract_technology_links(c_content)
			if len(links) == 0:
				page_count = -1
			else:
				technology_links += links
			page_count += 1
	except Exception:
		print "exception at " + str(page_count)

	return technology_links

def extract_technology_links(html_soup):
	links = []
	all_content_links = html_soup.findAll("a")
	for c_link in all_content_links:
		if "technology" in c_link["href"] and c_link["href"] not in links and 'searchresults' not in c_link["href"]:
			links.append(c_link["href"])

	return links

def get_technology_information(technology_links):
	logging.basicConfig(filename='inteum_warnings.log', level=logging.DEBUG)

	print "Opening the file..."
	file = open(sys.argv[2], 'w')
	num_added = 0

	for link in technology_links:
		web_published = ""

		tech_link = BASE_URL + link
		internal_number = ""
		tech_name = ""
		managed_by = []
		inventors = []
		publications = []
		categories = []
		ip = []
		data = ""
		keywords = []

		html = urlopen(tech_link).read()
		html_soup = BeautifulSoup(html, "lxml")
		body_content = html_soup.findAll('div', 'c_content')[0].tr.td

		######################################################
		# Extract case name
		######################################################

		try:
			tech_name = body_content.h1.string
		except Exception:
			logging.warning('could not find technology name')



		######################################################
		# Extract data, case id, web published date, patent info
		######################################################

		data = BeautifulSoup(str(body_content), "lxml").text.encode('utf-8', 'ignore')

		# if 'Web Published' in data:
		# 	web_published = data.split('Web Published:')[1]
		try:
			if 'Intellectual Property' in data:
				intellectual_property = data.split('Intellectual Property')[1]
				i = intellectual_property.split('\xc2')[0]
				ip.append(i)
		except:
			logging.warning('could not find ip')

		try:
			if 'ASSOCIATED PUBLICATIONS' in data or 'Associated Publications' in data or 'associated publications' in data:
				intellectual_property = data.split('ASSOCIATED PUBLICATIONS')[1]
				i = intellectual_property.split

		if 'Patent Information' in data:
			beg_index = data.find('Patent No.')
			end_index = data.find('File Date')
			try:
				additional_ip = data[beg_index+10:end_index]
				if additional_ip != '\n':
					ip.append(data[beg_index + 10:end_index])
			except Exception:
				logging.warning("had issues with patent extraction")



		######################################################
		# Side bar items: inventor, manager, categories
		######################################################

		try:
			category_content = html_soup.findAll('div', 'c_tp_description')[1]
			cats = category_content.text.split('\n')
			for item in cats:
				if item != '' and 'Category' not in item:
					categories.append(str(item))
		except Exception:
			logging.warning('could not find categories')

		try:
			contact_content = html_soup.findAll('div', 'c_tp_contact')[0]
			contacts = contact_content.text.split('\n')
			for item in contacts:
				if 'For Information' in item or 'for information' in item or 'For information' in item:
					pass
				elif item != '':
					try:
						managed_by.append(str(item).strip())
					except Exception:
						managed_by.append(item.encode('utf-8', 'ignore'))
		except Exception:
			logging.warning('could not find contacts')

		new_content = html_soup.findAll('div')
		for item in new_content:
			if 'Inventors' in item.text:
				if item.next_sibling != None:
					if item.next_sibling.next_sibling != None:
						if item.next_sibling.next_sibling.next_sibling != None:
							try:
								inventors.append(item.next_sibling.next_sibling.next_sibling.next_sibling.text.strip())
							except Exception:
								inventors.append(item.next_sibling.next_sibling.next_sibling.next_sibling.text.encode('utf-8', 'ignore'))

			if 'Keywords' in item.text:
				if item.next_sibling != None:
					if item.next_sibling.next_sibling != None:
						if item.next_sibling.next_sibling.next_sibling != None:
							try:
								keywords = item.next_sibling.next_sibling.next_sibling.next_sibling.text.split('\n')
							except Exception:
								keywords = [item.next_sibling.next_sibling.next_sibling.next_sibling.text.encode('utf-8', 'ignore')]

		try:
			print_string = "({univ}, {url}, {int_num}, {tech_name}, {managed_by}, {inventors}, {publications}, {categories}, {ip}, {data}, {keywords})\n".format(
				univ=UNIVERSITY, url=tech_link, int_num=internal_number, tech_name=tech_name, managed_by=managed_by,
				inventors=inventors, publications=publications, categories=categories, ip=ip, data=data, keywords=keywords)
			file.write(print_string)
			num_added += 1
		except Exception:
			pass



	file.close()
	print num_added

def main():
	pages = loop_through_all_pages()
	print pages
	# pages = ['/technology/23583', '/technology/23582', '/technology/23581', '/technology/23580', '/technology/23579', '/technology/23566', '/technology/23565', '/technology/23552', '/technology/23526', '/technology/23525', '/technology/23524', '/technology/23523', '/technology/23522', '/technology/23509', '/technology/23427', '/technology/23291', '/technology/23057', '/technology/23055', '/technology/23013', '/technology/23012', '/technology/23011', '/technology/23010', '/technology/23009', '/technology/23008', '/technology/23007', '/technology/23003', '/technology/23002', '/technology/22969', '/technology/22967', '/technology/22966', '/technology/22965', '/technology/22959', '/technology/22958', '/technology/22955', '/technology/22883', '/technology/22882', '/technology/22881', '/technology/22843', '/technology/22781', '/technology/22780', '/technology/22757', '/technology/22735', '/technology/22715', '/technology/22646', '/technology/22645', '/technology/22598', '/technology/22597', '/technology/22596', '/technology/22595', '/technology/22589', '/technology/22493', '/technology/22429', '/technology/22416', '/technology/22410', '/technology/22395', '/technology/22354', '/technology/22352', '/technology/22350', '/technology/22306', '/technology/22294', '/technology/22278', '/technology/22225', '/technology/22224', '/technology/22215', '/technology/22214', '/technology/22190', '/technology/22155', '/technology/22154', '/technology/22153', '/technology/22152', '/technology/22151', '/technology/22150', '/technology/22122', '/technology/22121', '/technology/22120', '/technology/22109', '/technology/22108', '/technology/22077', '/technology/21991', '/technology/21972', '/technology/21934', '/technology/21931', '/technology/21868', '/technology/21867', '/technology/21866', '/technology/21862', '/technology/21824', '/technology/21759', '/technology/21713', '/technology/21651', '/technology/21618', '/technology/21614', '/technology/21605', '/technology/21604', '/technology/21603', '/technology/21526', '/technology/21525', '/technology/21524', '/technology/21523', '/technology/21521', '/technology/21520', '/technology/21518', '/technology/21509', '/technology/21508', '/technology/21469', '/technology/21468', '/technology/21467', '/technology/21466', '/technology/21458', '/technology/21406', '/technology/21405', '/technology/21404', '/technology/21403', '/technology/21376', '/technology/21375', '/technology/21356', '/technology/21355', '/technology/21354', '/technology/21351', '/technology/21350', '/technology/21315', '/technology/21271', '/technology/21268', '/technology/21267', '/technology/21265', '/technology/21264', '/technology/21263', '/technology/21199', '/technology/21177', '/technology/21174', '/technology/21171', '/technology/21170', '/technology/21168', '/technology/21062', '/technology/20987', '/technology/20950', '/technology/20940', '/technology/20868', '/technology/20863', '/technology/20860', '/technology/20809', '/technology/20808', '/technology/20807', '/technology/20788', '/technology/20693', '/technology/20575', '/technology/20570', '/technology/20483', '/technology/20475', '/technology/20473', '/technology/20467', '/technology/20460', '/technology/20454', '/technology/20266', '/technology/20265', '/technology/20235', '/technology/20233', '/technology/20232', '/technology/20206', '/technology/20203', '/technology/20199', '/technology/20198', '/technology/20197', '/technology/20196', '/technology/20194', '/technology/20193', '/technology/20130', '/technology/20020', '/technology/20017', '/technology/20014', '/technology/20013', '/technology/20000', '/technology/19986', '/technology/19985', '/technology/19946', '/technology/19736', '/technology/19730', '/technology/19701', '/technology/19510', '/technology/19509', '/technology/19502', '/technology/19500', '/technology/19465', '/technology/19460', '/technology/19446', '/technology/19415', '/technology/19366', '/technology/19349', '/technology/19348', '/technology/19346', '/technology/19294', '/technology/19283', '/technology/19282', '/technology/19281', '/technology/19266', '/technology/19241', '/technology/19240', '/technology/19239', '/technology/19238', '/technology/19237', '/technology/19235', '/technology/19232', '/technology/19231', '/technology/19230', '/technology/19217', '/technology/19036', '/technology/15453', '/technology/15322', '/technology/15064', '/technology/14967', '/technology/14966', '/technology/14965', '/technology/14953', '/technology/14952', '/technology/14949', '/technology/14947', '/technology/14946', '/technology/14944', '/technology/14943', '/technology/14926', '/technology/14925', '/technology/14922', '/technology/14921', '/technology/14915', '/technology/14914', '/technology/14913', '/technology/14912', '/technology/14905', '/technology/14904', '/technology/14903', '/technology/14902', '/technology/14901', '/technology/14823', '/technology/14811', '/technology/14809', '/technology/14794', '/technology/8624', '/technology/8623', '/technology/8622', '/technology/8621', '/technology/8620', '/technology/8619', '/technology/8618', '/technology/8615', '/technology/8614', '/technology/8613', '/technology/8612', '/technology/8611', '/technology/8610', '/technology/8609', '/technology/8608', '/technology/8607', '/technology/8606', '/technology/8605', '/technology/8603', '/technology/8602', '/technology/8601', '/technology/8600', '/technology/8598', '/technology/8597', '/technology/8596', '/technology/8595', '/technology/8594', '/technology/8593', '/technology/8592', '/technology/8591', '/technology/8590', '/technology/8589', '/technology/8588', '/technology/8587', '/technology/8586', '/technology/8582', '/technology/8581', '/technology/8580', '/technology/8579', '/technology/8578', '/technology/8577', '/technology/8576', '/technology/8575', '/technology/8574', '/technology/8573', '/technology/8572', '/technology/8571', '/technology/8570', '/technology/8569', '/technology/8568', '/technology/8567', '/technology/8566', '/technology/8565', '/technology/8564', '/technology/8563', '/technology/8562', '/technology/8561', '/technology/8560', '/technology/8559', '/technology/8555', '/technology/8554', '/technology/8553', '/technology/8552', '/technology/8551', '/technology/8550', '/technology/8549', '/technology/8548', '/technology/8547', '/technology/8546', '/technology/8544', '/technology/8543', '/technology/8542', '/technology/8541', '/technology/8540', '/technology/8539', '/technology/8538', '/technology/8537', '/technology/8536', '/technology/8535', '/technology/8534']
	get_technology_information(pages)

if __name__ == "__main__":
	main()